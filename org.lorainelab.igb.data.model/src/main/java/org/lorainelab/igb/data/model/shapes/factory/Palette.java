package org.lorainelab.igb.data.model.shapes.factory;

/**
 *
 * @author dcnorris
 */
public class Palette {

    public static final String PRIMARY_COLOR = "#FF9800";
    public static final String DARK_PRIMARY_COLOR = "#F57C00";
    public static final String LIGHT_PRIMARY_COLOR = "#FFE0B2";
    public static final String TEXT_PRIMARY_COLOR = "#212121";
    public static final String ACCENT_COLOR = "#00BCD4";
    public static final String PRIMARY_TEXT_COLOR = "#212121";
    public static final String SECONDARY_TEXT_COLOR = "#727272";
    public static final String DIVIDER_COLOR = "#B6B6B6";

}
